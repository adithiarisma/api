<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembeliansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelians', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('jumlah_pemesanan');
            $table->unsignedBigInteger('products_id');
            $table->unsignedBigInteger('pesanans_id');
            $table->timestamps();
            $table
                ->foreign('products_id')
                ->references('id')
                ->on('products');
            $table
                ->foreign('pesanans_id')
                ->references('id')
                ->on('pesanans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelians');
    }
}
