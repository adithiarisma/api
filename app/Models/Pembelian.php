<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    use HasFactory;
    protected $table = 'pembelians';
    protected $fillable = ['jumlah_pemesanan', 'products_id', 'pesanans_id'];

    public function products()
    {
        return $this->belongsTo('App\Models\Products', 'products_id', 'id');
    }
    public function pesanans()
    {
        return $this->belongsTo('App\Models\Pesanan', 'pesanans_id', 'id');
    }
}
